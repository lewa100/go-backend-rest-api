package main

import (
	"context"
	"fmt"
	"go-backend-rest-api/config"
	"go-backend-rest-api/helper"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"

	"github.com/sirupsen/logrus/hooks/writer"

	log "github.com/sirupsen/logrus"
)

var (
	cfg         = config.Cfg
	hl          *log.Entry
	BuildCommit = ""
)

func confLogs(fOut, fErr *os.File) {
	//Set groups for logout
	//Stderr helper
	log.AddHook(&writer.Hook{ // Send logs with level higher than warning to stderr
		Writer: fErr,
		LogLevels: []log.Level{
			log.PanicLevel,
			log.FatalLevel,
			log.ErrorLevel,
			log.WarnLevel,
		},
	})
	//Stdout helper
	log.AddHook(&writer.Hook{ // Send info and debug logs to stdout
		Writer: fOut, //io.MultiWriter(fOut, os.Stdout)
		LogLevels: []log.Level{
			log.InfoLevel,
		},
	})
}

func setLog() {
	//Установка режима логирования(debug)
	log.SetFormatter(&log.JSONFormatter{})
	standardFields := log.Fields{
		"package": "main",
	}
	hl = log.WithFields(standardFields)
}

func main() {
	setLog()
	// инициализируем
	// конфиг
	if err := config.ConfigLoad("config.toml"); err != nil {
		hl.Errorf("config file not load: %s", err)
	}

	helper.SetLog(cfg.Log)
	//Create helper files
	log.SetOutput(ioutil.Discard) // Send all logs to nowhere by default
	defer log.SetOutput(os.Stdout)

	err := helper.CreateDirIsNotExist(cfg.DirLogs)
	if err != nil {
		hl.Errorf("create folder with error: %s", err)
	}
	fOut, err := os.OpenFile(cfg.DirLogs+"access.helper", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		hl.Fatalf("error opening file: %v", err)
	}
	defer fOut.Close()

	fErr, err := os.OpenFile(cfg.DirLogs+"error.helper", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		hl.Fatalf("error opening file: %v", err)
	}
	defer fErr.Close()
	confLogs(fOut, fErr)

	//определяем путь исполняемого файла
	app, _ := filepath.Abs(filepath.Dir("."))
	hl.WithFields(log.Fields{
		"path app":     app,
		"build commit": BuildCommit,
	}).Debug("app info")

	fmt.Println("path app:", app)
	fmt.Println("build commit:", BuildCommit)
	//Установка слушателя OS сигналов
	helper.SetOSSignals()

	// создаем контекст завершения и канал сигнализации завершения
	ctx, cancel := context.WithCancel(context.Background())
	stopped := make(chan struct{})
	// запускаем обработчик команд
	go run(ctx, stopped)

	// ожидаем сигнала от ОС
	signal := helper.WaitForOSSignal()
	hl.WithFields(log.Fields{"signal": signal.String()}).Warn("OS signal")
	cancel()
	<-stopped
	hl.Warn("Stopped")
}

// run запускает все заданные команды на исполнение
func run(ctx context.Context, stopped chan struct{}) {
	waiter := new(sync.WaitGroup)
	//waiter.Add(1)
	//go frontend()
	//waiter.Add(1)
	//go backend()
	//waiter.Add(1)
	//go db()
	waiter.Wait()
	stopped <- struct{}{}
}
