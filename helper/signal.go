package helper

import (
	"os"
	"os/signal"
	"syscall"
)

var done = make(chan os.Signal, 1)

// SetOSSignals настраивает сигналы ОС
func SetOSSignals() {
	signal.Notify(done, syscall.SIGTERM)
	signal.Notify(done, syscall.SIGINT)
	//signal.Notify(done, syscall.SIGKILL)
}

// WaitForOSSignal ожидает сигнал от ОС
func WaitForOSSignal() os.Signal {
	return <-done
}
