package helper

import (
	"testing"

	"github.com/stretchr/testify/require"

	"github.com/stretchr/testify/assert"
)

type structDirRead struct {
	folder   string
	filename []string
	fileList []string
	error    string
}

var dataDirRead = []structDirRead{
	{
		folder:   "testDir/",
		filename: []string{"test"},
		fileList: []string{"testDir/test"},
	},
	{
		folder:   "testDir/",
		filename: []string{"test", "test3"},
		fileList: []string{"testDir/test", "testDir/test3"},
	},
	{
		folder:   "testDir/",
		filename: []string{"test4"},
		fileList: nil,
		error:    "File not found: test4",
	},
}

func TestDirReader(t *testing.T) {
	t.Run("TestFind", func(t *testing.T) {
		for _, v := range dataDirRead {
			b, err := DirReader(v.folder, v.filename...)
			if err != nil {
				assert.EqualError(t, err, v.error, "unexpected name: expected %v error: %v", v.error, err.Error())
				return
			}
			require.NoError(t, err)
			assert.Equal(t, b, v.fileList, "unexpected name: expected %v fileList: %v", v.fileList, b)
		}
	})
}
