package helper

import "strings"

func Find(slice []string, val string) (int, bool) {
	for i, item := range slice {
		if strings.ToLower(item) == strings.ToLower(val) {
			return i, true
		}
	}
	return -1, false
}

func Split(str, char string) ([]string, int) {
	parts := strings.Split(str, char)
	return parts, len(parts)
}
