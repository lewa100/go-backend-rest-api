package helper

import log "github.com/sirupsen/logrus"

func SetLog(lvl string) {
	// parse string, this is built-in feature of logrus
	ll, err := log.ParseLevel(lvl)
	if err != nil {
		ll = log.DebugLevel
	}
	// set global logging level
	log.SetLevel(ll)
}
