package helper

import (
	"fmt"
	"os"
	"path"

	"github.com/pkg/errors"
)

//DirReader get fileList from folder
func DirReader(folder string, names ...string) (fileList []string, err error) {
	var file *os.File
	file, err = os.Open(path.Dir(folder))
	if err != nil {
		return nil, err
	}
	defer file.Close()

	list, _ := file.Readdirnames(0)

	for _, n := range names {
		_, found := Find(list, n)
		if found {
			filename := path.Dir(folder) + "/" + n
			fileList = append(fileList, filename)
		} else {
			return nil, errors.Errorf("File not found: %s", n)
		}
	}

	return
}

func CreateDirIsNotExist(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {

		err := os.Mkdir(path, 0755)
		if err != nil {
			return err
		}
		fmt.Println("Directory created", path)
	} else {

		fmt.Println("Directory already exists", path)
	}
	return nil
}
