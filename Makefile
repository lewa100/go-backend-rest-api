BUILD_COMMIT := $(shell git log --format="%H" -n 1)

build:
	go build -o . -ldflags="-X 'main.BuildCommit=$(BUILD_COMMIT)'" ./...

test:
	go test ./... -cover > coverage

check:
	golangci-lint run ./...
