package config

import (
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type Opts struct {
	Log string
	Reader
	DirLogs string
	Timeout time.Duration
}

type Reader struct {
	FieldsPerRecord  int
	TrimLeadingSpace bool
	Comma            []rune
}

var (
	Cfg = &Opts{}
	hl  *log.Entry
)

func setLog() {
	standardFields := log.Fields{
		"package": "config",
	}
	hl = log.WithFields(standardFields)
}

func ConfigLoad(cfgName string) (err error) {
	setLog()
	// считываем опции
	viper.SetConfigName(cfgName)
	viper.SetConfigType("toml")
	viper.AddConfigPath(".")

	setDefaults()
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			hl.WithFields(log.Fields{"config": cfgName}).Warnf("configuration file %s not found", cfgName)
		} else {
			hl.WithFields(log.Fields{"config": cfgName}).Errorf("unable to load configuration file %s: %s", cfgName, err)
		}
	}

	cfgLog()
	cfgReader()
	return
}

func cfgReader() {
	Cfg.Reader = Reader{
		FieldsPerRecord:  viper.GetInt("reader.fieldsPerRecord"),
		TrimLeadingSpace: viper.GetBool("reader.trimLeadingSpace"),
		Comma:            []rune(viper.GetString("reader.comma")),
	}
	Cfg.DirLogs = viper.GetString("logs.dirForLogs")
	Cfg.Timeout = viper.GetDuration("ttl.timeout") * time.Second
}

func cfgLog() {
	Cfg.Log = viper.GetString("logging.level")
}

func setDefaults() {
	viper.SetDefault("ttl.timeout", 10)
	viper.SetDefault("reader.fieldsPerRecord", 4)
	viper.SetDefault("reader.trimLeadingSpace", true)
	viper.SetDefault("reader.comma", ",")

	// Log
	viper.SetDefault("log.level", "debug")
	viper.SetDefault("log.dirForLogs", "logs/")
}
